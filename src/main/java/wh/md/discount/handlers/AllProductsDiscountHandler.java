package wh.md.discount.handlers;

import java.math.BigDecimal;

import wh.md.discount.domain.DiscountRule;
import wh.md.discount.domain.Product;

public class AllProductsDiscountHandler implements DiscountRuleHandler {

    @Override
    public BigDecimal apply(Product product, DiscountRule productDiscountRule) {
        if (product.getCount() >= productDiscountRule.getRequiredCount()) {
            return productDiscountRule.getDiscountedPrice().multiply(BigDecimal.valueOf(product.getCount()));
        } else {
            return product.getBasicPrice().multiply(BigDecimal.valueOf(product.getCount()));
        }
    }
}
