package wh.md.discount.domain;

public enum RuleType {
    X_PRODUCT_DISCOUNT,
    ALL_PRODUCTS_DISCOUNT
}
