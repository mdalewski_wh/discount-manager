package wh.md.discount.domain;

import java.math.BigDecimal;

import lombok.Value;

@Value
public class DiscountRule {

    private String productName;
    private Integer requiredCount;
    private BigDecimal discountedPrice;
    private RuleType ruleType;

}
