package wh.md.discount.domain;

import java.math.BigDecimal;

import lombok.Value;

@Value
public class Product {

    private final String name;
    private final BigDecimal basicPrice;
    private final Integer count;
}
