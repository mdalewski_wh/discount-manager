package wh.md.discount.domain;

import java.math.BigDecimal;

import lombok.Value;

@Value
public class TotalPrice {
    private final BigDecimal price;
}
