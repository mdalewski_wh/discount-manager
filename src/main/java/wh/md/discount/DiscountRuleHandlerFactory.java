package wh.md.discount;

import wh.md.discount.domain.RuleType;
import wh.md.discount.handlers.AllProductsDiscountHandler;
import wh.md.discount.handlers.DiscountRuleHandler;
import wh.md.discount.handlers.SingleProductDiscountHandler;

public class DiscountRuleHandlerFactory {

    DiscountRuleHandler getDiscountRualeHandler(RuleType ruleType) {
        switch (ruleType) {
            case X_PRODUCT_DISCOUNT -> {
                return new SingleProductDiscountHandler();
            }
            case ALL_PRODUCTS_DISCOUNT -> {
                return new AllProductsDiscountHandler();
            }
            default -> throw new RuntimeException();
        }
    }
}
