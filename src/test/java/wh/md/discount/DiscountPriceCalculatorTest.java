package wh.md.discount;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import wh.md.discount.domain.DiscountRule;
import wh.md.discount.domain.Product;
import wh.md.discount.domain.RuleType;
import wh.md.discount.domain.TotalPrice;


class DiscountPriceCalculatorTest {

    @Test
    void appleMatchingDiscountTest() {
        List<DiscountRule> discountRules = new ArrayList<>();
        discountRules.add(new DiscountRule("Apple", 3, new BigDecimal("1.00"), RuleType.X_PRODUCT_DISCOUNT));

        PriceCalculator priceCalculator = new DiscountPriceCalculator();
        priceCalculator.setDiscountRules(discountRules);

        List<Product> products = new ArrayList<>();
        products.add(new Product("Apple", new BigDecimal("2.00"), 3));

        TotalPrice totalPrice = priceCalculator.calculatePrice(products);

        assertEquals(new BigDecimal("5.00"), totalPrice.getPrice());
    }

    @Test
    void appleNotMatchingDiscountTest() {
        List<DiscountRule> discountRules = new ArrayList<>();
        discountRules.add(new DiscountRule("Apple", 3, new BigDecimal("1.00"), RuleType.X_PRODUCT_DISCOUNT));

        PriceCalculator priceCalculator = new DiscountPriceCalculator();
        priceCalculator.setDiscountRules(discountRules);

        List<Product> products = new ArrayList<>();
        products.add(new Product("Apple", new BigDecimal("2.00"), 2));

        TotalPrice totalPrice = priceCalculator.calculatePrice(products);

        assertEquals(new BigDecimal("4.00"), totalPrice.getPrice());
    }

    @Test
    void positiveTest() {
        List<DiscountRule> discountRules = new ArrayList<>();
        discountRules.add(new DiscountRule("Apple", 3, new BigDecimal("1.00"), RuleType.X_PRODUCT_DISCOUNT));
        discountRules.add(new DiscountRule("Orange", 5, new BigDecimal("3.00"), RuleType.ALL_PRODUCTS_DISCOUNT));

        PriceCalculator priceCalculator = new DiscountPriceCalculator();
        priceCalculator.setDiscountRules(discountRules);

        List<Product> products = new ArrayList<>();
        products.add(new Product("Apple", new BigDecimal("2.00"), 5));
        products.add(new Product("Orange", new BigDecimal("5.00"), 5));
        products.add(new Product("Bannana", new BigDecimal("3.00"), 3));

        TotalPrice totalPrice = priceCalculator.calculatePrice(products);
        assertEquals(new BigDecimal("33.00"), totalPrice.getPrice());
    }
}